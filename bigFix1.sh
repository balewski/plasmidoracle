#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

jobPath='/global/cscratch1/sd/balewski/plasmid_sum/4g'
echo path=$jobPath

k=0

for dirN in $( ls $jobPath)  ; do   
    fullPath=$jobPath/$dirN
    echo work on  $fullPath  
    ./train_Plasmid.py --dataPath dataSmall --seedWeights  $fullPath -X
    cp -rp out/assayer4.model.h5  $fullPath
    exit
    idx=`echo $dirN | cut -f2 -d-`
    kfold=$(( $idx % 10 ))
    #if [ $idx -ne 21 ] ; then  echo skip $idx, $dirN; continue; fi
    k=$[ $k + 1 ]
    
    #if [ $k -gt $dayB ] ; then echo skip day=$k an later; break; fi
    
    echo " ==================    processing kfold=$kfold "
    time ./kfold_one_eval.py  --kfoldOffset $kfold  --seedModel $fullPath --arrIdx $idx  # --dataPath dataSmall/
    
done

exit
 ==================    processing kfold=7 
./kfold_one_eval.py --kfoldOffset 8 --seedModel /global/cscratch1/sd/balewski/kinase10_sum/10b/kinase10b-18 --arrIdx 18   --dataPath dataSmall/
