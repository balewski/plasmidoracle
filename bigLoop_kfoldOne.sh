#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

jobPath='/global/cscratch1/sd/balewski/plasmid_sum/4g'
echo path=$jobPath

k=0

for dirN in $( ls $jobPath)  ; do   
    idx=`echo $dirN | cut -f2 -d-`
    kfold=$(( $idx % 5 ))
    if [ $idx -le 10 ] ; then  echo skip $idx, $dirN; continue; fi
    k=$[ $k + 1 ]
    
    #if [ $k -gt $dayB ] ; then echo skip day=$k an later; break; fi
    fullPath=$jobPath/$dirN
    echo work on  $fullPath  kfold=$kfold
    
    echo " ==================    processing kfold=$kfold "
    time ./kfold_one_eval.py  --kfoldOffset $kfold  --seedModel $fullPath --arrIdx $idx   --dataPath dataBig/ --events 512000
    #exit
done

exit
 ==================    processing kfold=7 
./kfold_one_eval.py --kfoldOffset 8 --seedModel /global/cscratch1/sd/balewski/kinase10_sum/10b/kinase10b-18 --arrIdx 18   --dataPath dataSmall/
